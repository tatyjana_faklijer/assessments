var Gigasecundum = require('./gigasecond');

describe("Gigasecundum", function() {

  it("test 1", function() {
    var gs = new Gigasecundum(new Date(1984, 4, 13));
    var expectedDate = new Date(2015, 12, 21);
    expect(gs.date()).toEqual(expectedDate);
  });
});
