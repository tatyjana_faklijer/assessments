'use strict'

function Gigasecundum(date) {
  this.birthday = date;
};

Gigasecundum.prototype.date = function() {
  let birthTime = this.birthday.getTime();
  let gigaSecDay = new Date(birthTime + 1000000000000);
  gigaSecDay.setSeconds(0);
  gigaSecDay.setMinutes(0);
  gigaSecDay.setHours(0);
  return gigaSecDay;
};

module.exports = Gigasecundum;

console.log(Gigasecundum(1984, 4, 13));
